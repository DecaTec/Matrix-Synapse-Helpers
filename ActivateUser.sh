#!/bin/bash

#
# Activates a user
#

# Requirements:
# - The script has to be executed on the same machine where Matrix Synapse is installed.
# - A new password for the user must be specified.
# - For authentication, you will need the access token of your (admin) account.
#   You can obtain it in your Element client: "All settings" > "Help & About" > scroll down > click on "<click to reveal>" to get your access token.
#   Copy this token and paste it further down (variable "token").
#

token=""
protocol="http"
port="8008"

if [ -z "$token" ]
then
      read -pr "Enter your access token: " token
      echo ""
fi

read -pr "Enter the user which should be activated: (e.g. @test:matrix.mydomain.com): " user_to_activate
echo ""
read -spr "Enter the new passwort for user: " new_password
echo ""

# User lower case only
user_to_activate="${user_to_activate,,}"

# Activate user
echo "Activate user $user_to_activate..."
echo ""
curlUrl="$protocol://localhost:$port/_synapse/admin/v2/users/$user_to_activate?access_token=$token"

echo "$curlUrl"
curl -k -X PUT "$curlUrl" -H 'Content-Type: application/json' -d '{"deactivated": false, "password":"'"$new_password"'"}'
echo ""
echo "Done"
echo ""