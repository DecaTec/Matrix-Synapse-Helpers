#!/bin/bash

#
# Changes a password for a user
#

# Requirements:
# - The script has to be executed on the same machine where Matrix Synapse is installed.
# - For authentication, you will need the access token of your (admin) account.
#   You can obtain it in your Element client: "All settings" > "Help & About" > scroll down > click on "<click to reveal>" to get your access token.
#   Copy this token and paste it further down (variable "token").
# - The user (@username:mydomain.com) whose password should be changed
#

token=""
protocol="http"
port="8008"

if [ -z "$token" ]
then
      read -p "Enter your access token: " token
      echo ""
fi

read -pr "Enter the user whose password should be changed: (e.g. @test:matrix.mydomain.com): " user_to_change_password
echo ""

read -spr "Enter the NEW password: " new_password
echo ""

read -spr "Confirm the new password: " new_password2
echo ""

if [ "$new_password" != "$new_password2" ]; then
      echo "Error: The passwords do not match"
      echo "The password was NOT changed"!
      exit 1
fi

# User lower case only
user_to_change_password="${user_to_change_password,,}"

# Change users's password
echo "Change password for user $user_to_change_password..."
echo ""
curlUrl="$protocol://localhost:$port/_synapse/admin/v2/users/$user_to_change_password?access_token=$token"
curl -k -X PUT "$curlUrl" -H 'Content-Type: application/json' -d '{"password": "'"$new_password"'"}'
echo ""
echo "Done"
echo ""