#!/bin/bash

#
# Deactivates a user
#

# IMPORTANT: You cannot delete a user from a Matrix server, a user can just get deactivated.
# Requirements:
# - The script has to be executed on the same machine where Matrix Synapse is installed.
# - For authentication, you will need the access token of your (admin) account.
#   You can obtain it in your Element client: "All settings" > "Help & About" > scroll down > click on "<click to reveal>" to get your access token.
#   Copy this token and paste it further down (variable "token").
#

token=""
protocol="http"
port="8008"

if [ -z "$token" ]
then
      read -p "Enter your access token: " token
      echo ""
fi

read -pr "Enter the user which should be deactivated: (e.g. @test:matrix.mydomain.com): " user_to_deactivate
echo ""

# User lower case only
user_to_deactivate="${user_to_deactivate,,}"

# Deactivate user
echo "Deactivate user $user_to_deactivate..."
echo ""
curlUrl="$protocol://localhost:$port/_synapse/admin/v1/deactivate/$user_to_deactivate?access_token=$token"
curl -k -X POST "$curlUrl" -H 'Content-Type: application/json' -d '{"erase": true}'
echo ""
echo "Done"
echo ""