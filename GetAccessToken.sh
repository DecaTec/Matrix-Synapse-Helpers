#!/bin/bash

#
# Gets the access token of a Matrix Synapse account using curl.
#

# The following variables have to be set individually.
serverDomain="matrix.mydomain.com"
user="MYUSER"
password="MYPASSWORD"

json="{\"type\":\"m.login.password\", \"user\":\"$user\", \"password\":\"$password\"}"
response=$(curl --silent -w "\n%{http_code}" -XPOST -d "$json" "https://$serverDomain/_matrix/client/r0/login")
http_code=$(tail -n1 <<< "$response")
content=$(sed '$ d' <<< "$response")

if [[ "$http_code" -ne 200 ]] ; then
	echo "Error: $content"
	exit 1
fi

echo ""
echo "Your access token:"
echo ""

echo "$content"

echo ""
echo ""
