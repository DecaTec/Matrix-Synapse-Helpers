#!/bin/bash

#
# Lists all users of the given instance.
#

# Requirements:
# - The script has to be executed on the same machine where Matrix Synapse is installed.
# - Python 2.6+ has to be installed on the system.
# - For authentication, you will need the access token of your (admin) account.
#   You can obtain it in your Element client: "All settings" > "Help & About" > scroll down > click on "<click to reveal>" to get your access token.
#   Copy this token and paste it further down (variable "token").
#

token=""
protocol="http"
ort="8008"

if [ -z "$token" ]
then
      read -pr "Enter your access token: " token
      echo ""
fi

# List users
echo "Retrieving users..."
echo ""
curlUrl="$protocol://localhost:$port/_synapse/admin/v2/users?access_token=$token"
output=$(curl -k "$curlUrl")
echo ""
echo "$output" | python -m json.tool
echo ""
echo "Done"
echo ""