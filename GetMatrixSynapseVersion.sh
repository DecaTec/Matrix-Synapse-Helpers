#!/bin/bash

#
# Gets the version of a Matrix Synapse installation
#

# The following variables have to be set individually.
serverDomain="matrix.mydomain.com"

echo "Getting version..."
echo ""
curlUrl="https://$serverDomain/_matrix/federation/v1/version"
version=$(curl -sS $curlUrl | grep '"version":' |  sed -E 's/.*"([^"]+)".*/\1/')
echo "Matrix Synapse version: $version"
echo ""
echo "Done"
echo ""
