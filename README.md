[![MIT License](https://img.shields.io/badge/license-MIT-green)](https://codeberg.org/DecaTec/Matrix-Synapse-Helpers/src/branch/main/LICENSE)
[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=jr%40decatec%2ede&lc=US&item_name=Matrix%20Synapse%20Helpers&no_note=1&no_shipping=1&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted)

# Helper scripts for Matrix Synapse

Some useful scripts utilizing the API of Matrix Synapse in order to manage different tasks:

* `ActivateUser.sh`: Activates the given user and assigns a new password.
* `ChangeUserPassword.sh`: Changes the password of the given user.
* `CleanupMedia.sh`: Deletes old local and remote media.
* `DeactivateUser.sh`: Deactivates the given user.
* `GetAccessToken.sh`: Gets the access token with curl.
* `GetAllUsers.sh`: Lists all users.
* `GetMatrixSynapseVersion.sh`: Gets the version of a Matrix Synapse instance.
* `UpdateRoomVersion.sh`: Updates a room to the given room version.