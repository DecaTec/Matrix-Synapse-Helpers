#!/bin/bash

#
# Update room version
#

# Requirements:
# - The script has to be executed on the same machine where Matrix Synapse is installed.
# - You need the room ID of the room to update. You can find it in the room's settings under "advanced" (internal room ID).
# - For authentication, you will need the access token of your (admin) account.
#   You can obtain it in your Element client: "All settings" > "Help & About" > scroll down > click on "<click to reveal>" to get your access token.
#   Copy this token and paste it further down (variable "token").
#

token=""
newRoomVersion=""
roomId=""
protocol="http"
port="8008"

if [ -z "$newRoomVersion" ]
then
      read -pr "Enter the new room version (e.g. 6): " newRoomVersion
      echo ""
fi


if [ -z "$token" ]
then
      read -pr "Enter your access token: " token
      echo ""
fi

# Delete local media
echo "Update room version of room $roomId..."
echo ""
curlUrl="$protocol://localhost:$port/_matrix/client/r0/rooms/$roomId/upgrade?access_token=$token"
curl -k -X POST "$curlUrl" -H 'Content-Type: application/json' -d '{"new_version": "'"$newRoomVersion"'"}'
echo ""
echo "Done"
echo ""